# Record Collection 

<img src="icon.png"  width="150" height="150">


## Motivación  

Tener una colección de música en formatos físicos se puede volver una actividad complica a la hora de catalogar y encontrar esos discos que tanto queremos. Por esto surgió la idea de hacer una aplicación mobile en la cual podremos catalogar nuestro discos de forma rápida y buscar esos que nos faltan en la sección de publicaciones para completar nuestra colección.


## Features  

**Buscar**  
Buscar todas las ediciones en formato físico en la base de datos de Discogs

**Colección**  
Gestionar la colección agregando y eliminando las ediciones encontradas en la búsqueda


**Lista de deseados**  
Añadir discos a la lista de deseados

**Escanear código de barras**. 
Encuentra una edición de manera rápida escaneando el código de barras

**Publicaciones**  
Publicar los discos que ya no queres o buscá esos que te faltan en la sección de publicaciones

**Chat**  
Gestionar ventas  y compras por medio del chat


